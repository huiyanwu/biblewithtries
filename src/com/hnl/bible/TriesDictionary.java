package com.hnl.bible;

import java.util.HashMap;

import com.hnl.bible.TriesDictionary.Node;

public class TriesDictionary {

	static Node dictionary = new Node();


	public static void main(String[] args) {
		String[] words = {"abc","Nothing", "Gonna", "Change", "My", "Love", "For", "You", "I"};

		for(int i = 0; i < words.length; i++) {
			System.out.println("==============>i=" + i);
			dictionary.insertWord(words[i]);
			dictionary.print(dictionary);
		}

		for(int i = 0; i < words.length; i++) {
			boolean isExist = dictionary.search(words[i]);
			System.out.println("---------->isExist=" + isExist);
		}

	}

	

	public static Node getDictionary() {
		return dictionary;
	}



	public static void setDictionary(Node dictionary) {
		TriesDictionary.dictionary = dictionary;
	}



	public static class Node{

		HashMap<Character, Node> children = new HashMap<Character, Node>();
		boolean endWord = false;

		public void insertWord(String word) {

			int i, size = word.length();
			char[] chars = new char[size];

			for(i = 0; i < size; i++) {
				chars[i] = word.charAt(i);
			}

			insertWordWithChars(chars, 0, dictionary);
		}


		public void insertWordWithChars(char[] chars, int startIndex, Node node){

			int size = chars.length;

			if(node == null || startIndex >= size) {
				return;
			}

			Node nextNode = null;

			if(node.children.containsKey(chars[startIndex])) {
				nextNode = node.children.get(chars[startIndex]);
			} else {
				nextNode = new Node();
				node.children.put(chars[startIndex], nextNode);
			} 

			if(startIndex == size -1) {
				nextNode.endWord = true;
			}

			insertWordWithChars(chars, ++startIndex, nextNode);
		}

		public boolean search(String word) {

			int i, size = word.length();
			char[] chars = new char[size];

			for(i = 0; i < size; i++) {
				chars[i] = word.charAt(i);
			}

			return searchWithChars(chars, dictionary);			
		}

		public boolean searchWithChars(char[] chars, Node node) {

			int size = chars.length;

			if(node == null) {
				return false;
			}

			Node tempNode = node;
			int index = 0;

			while (tempNode != null && index < size) {
				tempNode = tempNode.children.get(chars[index++]);
			}

			if(tempNode == null) {
				return false;
			}

			if(index == size && tempNode.endWord) {
				return true;
			} else {
				return false;
			}

		}

		public void print(Node node) {
			if(node == null) {
				return;
			}

			StringBuilder sb = new StringBuilder("");
			System.out.println();
			for(char key : node.children.keySet()) {
				sb.append(key + " ");
			}
			System.out.println(sb.toString());

			for(char key : node.children.keySet()) {
				print(node.children.get(key));
			}
		}

	}

}
