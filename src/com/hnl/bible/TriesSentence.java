package com.hnl.bible;


import java.util.HashMap;
import java.util.Vector;

/**
 * This is a tries structure for sentences, for example Bible verses.
 * 
 * @author Richard Wu
 *
 */
public class TriesSentence {

	static Node dictionary = new Node();


	public static void main(String[] args) {
		String[] locations = {"1","2", "2", "4", "5", "6", "7", "8", "9"};
		String[] words = {"abc","Nothing", "Gonna", "Change", "My", "Love", "For", "You", "I"};

		for(int i = 0; i < words.length; i++) {
			System.out.println("==============>i=" + i);
			dictionary.insertVerse(locations[i], words[i]);
			dictionary.print(dictionary);
		}

		for(int i = 0; i < words.length; i++) {
			boolean isExist = dictionary.search(words[i]);
			System.out.println("---------->isExist=" + isExist);
		}

	}

	

	public static Node getDictionary() {
		return dictionary;
	}



	public static void setDictionary(Node dictionary) {
		TriesSentence.dictionary = dictionary;
	}



	/**
	 * Define a Node with the tries tree structure. 
	 * @author Richard
	 *
	 */
	public static class Node{

		Node parent = null;
		String word = "";
		HashMap<String, Node> children = new HashMap<String, Node>();
		String location = "";
		boolean endVerse = false;

		/**
		 * Insert a verse with Book and Verse Location into the tries.
		 * @param location
		 * @param verse
		 */
		public void insertVerse(String location, String verse) {
			
			String[] words = verse.split(" ");

			insertVerseWithWords(location, words, 0, dictionary);
		}


		public String toString() {
			return location + "    " + word;
		}
		
		/**
		 * Insert a verse with all the words into the tries with location information. 
		 * @param location
		 * @param words
		 * @param startIndex
		 * @param node
		 */
		public void insertVerseWithWords(String location, String[] words, int startIndex, Node node){

			int size = words.length;

			if(node == null || startIndex >= size) {
				return;
			}

			Node nextNode = null;

			if(node.children.containsKey(words[startIndex])) {
				nextNode = node.children.get(words[startIndex]);
			} else {
				nextNode = new Node();
				node.children.put(words[startIndex], nextNode);
				nextNode.parent = node; //for search purpose. 
				nextNode.word = words[startIndex];
			} 

			if(startIndex == size -1) {
				nextNode.endVerse = true;
				nextNode.location = location;
			}

			insertVerseWithWords(location, words, ++startIndex, nextNode);
		}

		/**
		 * Search a verse in a tries. 
		 * 
		 * @param verse
		 * @return
		 */
		public boolean search(String verse) {
			
			String[] words = verse.split(" ");

			return searchWithWords(words, dictionary);			
		}

		/**
		 * Search all the words in a verse in order. 
		 * 
		 * @param words
		 * @param node
		 * @return
		 */
		public boolean searchWithWords(String[] words, Node node) {

			int size = words.length;

			if(node == null) {
				return false;
			}

			Node tempNode = node;

			int index = 0;

			while (tempNode != null && index < size) {

				tempNode = tempNode.children.get(words[index++]);
			}
			
			if(tempNode == null) {
				return false;
			}

			if(index == size && tempNode.endVerse) {
				return true;
			} else {
				return false;
			}

		}

		/**
		 * Print all nodes in a tries. 
		 * @param node
		 */
		public void print(Node node) {
			if(node == null) {
				return;
			}

			if(node.endVerse) {
				System.out.println(node.location);
			}
			

			for(String key : node.children.keySet()) {
//				System.out.print(key + " ");
				print(node.children.get(key));
			}
			
		}
		
		/**
		 * Search last Node of a given location. 
		 * @param location
		 * @param node
		 * @return
		 */
		public Node searchByLocation(String location, Node node) {
			
			Node lastNode = null;
			
			if(node == null) {
				return null;
			}
			
			if(node.location.equals(location)) {
				return node;
			}
			
			for(String key : node.children.keySet()) {
				lastNode = searchByLocation(location, node.children.get(key));
				if(lastNode != null) {
					return lastNode;
				}
			}
			
			return lastNode;

		}
		
		/**
		 * Search verse in a given location. It is slow.
		 *  
		 * @param location
		 * @param node
		 * @return
		 */
		public String getVerseByLocation(String location, Node node) {
			
			Node locatedNode = searchByLocation(location, node);
			Node parent = locatedNode;
			
			if(locatedNode == null) {
				return "";
			}
			
			Vector<String> words = new Vector<String>();
			
			while (parent != null) {
				words.add(parent.word);
				parent = parent.parent;
			}
			
			int i, size = words.size();
			StringBuilder sb = new StringBuilder("");
			
			for(i = size -1; i >=0; i--) {
				sb.append(words.elementAt(i) + " ");
			}
			
			return sb.toString();
		}		
		
	}
	
	

}
