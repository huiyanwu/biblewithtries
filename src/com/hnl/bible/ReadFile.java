package com.hnl.bible;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.StringTokenizer;

public class ReadFile {

	private static final String FILENAME = "data\\bible\\bible.txt";
	private static final String VERSE_LOCATION = "data\\bible\\verseLocation.txt";
	private static final String SEARCH_VERSE = "data\\bible\\searchVerse.txt";
	
	private static final String TOKENS = " ,:\\.";

	public static void main(String[] args) {
		

		ReadFile readFile = new ReadFile();
		HashMap map = readFile.readWordFromFile();
		
		System.out.println(map.size());
		
		
	}
	
	public HashMap<String, String> readWordFromFile() {

		HashMap<String, String> map = new HashMap<String, String>();
		
		try (BufferedReader br = new BufferedReader(new FileReader(FILENAME))) {

			String sCurrentLine;
			StringTokenizer stok = null;
			String word = "";

			while ((sCurrentLine = br.readLine()) != null) {
				stok = new StringTokenizer(sCurrentLine, TOKENS);
				while(stok.hasMoreTokens()) {
					word = stok.nextToken();
					map.put(word, word);	
				}
				
//				System.out.println(sCurrentLine);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return map;

	}
	
	public HashMap<String, String> readVerseFromFile() {

		HashMap<String, String> map = new HashMap<String, String>();
		
		try (BufferedReader br = new BufferedReader(new FileReader(FILENAME))) {

			String verse = "";
			String location = "";
			String content = "";

			while ((verse = br.readLine()) != null) {
				location = getLocation(verse);
				content = verse.substring(location.length()).trim();
				map.put(location, content);	
				
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return map;

	}	
	
	public String getLocation(String verse) {
		if(verse == null || verse.trim().length() ==0) {
			return "";
		}
		int columIndex = verse.indexOf(":");

		if(columIndex == -1) {
			return "";
		}
		
		String[] dLines = {verse.substring(0, columIndex), verse.substring(columIndex)};
		
		String verseNum = dLines[1].substring(0, dLines[1].indexOf("\t"));
		String location = dLines[0] + verseNum;
		return location.trim();
	}
	
	public HashMap<String, String> readVerseLocationFromFile() {

		return readLineFromFile(VERSE_LOCATION);

	}	
		
	public HashMap<String, String> readSearchVersenFromFile() {
		
		return readLineFromFile(SEARCH_VERSE);

	}
	
	public HashMap<String, String> readLineFromFile(String fileName) {

		HashMap<String, String> map = new HashMap<String, String>();
		
		try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
			String location = "";

			while ((location = br.readLine()) != null) {
				map.put(location.trim(), location.trim());	
				
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return map;

	}	
}
