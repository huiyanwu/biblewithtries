package com.hnl.bible;

import java.util.HashMap;

import com.hnl.bible.TriesDictionary.Node;

public class ParseHelper {

	public static void main(String[] args) {
		ParseHelper parseHelper = new ParseHelper();

		parseHelper.testVerses();
		
	}
	/**
	 * Test the 
	 */
	public void testWords() {
		TriesDictionary.Node bible = parseBibleWord();
		
		bible.print(bible);
		String[] words = {"He", "David", "synagogue"};
		
		for(String word: words) {
			System.out.println(bible.search(word));
		}
	}
	
	public void testVerses() {
		TriesSentence.Node bibleVerse = parseBibleVerse();
		
		bibleVerse.print(bibleVerse);
		String[] verses = {"And out of the ground made the LORD God to grow every tree that is pleasant to the sight, and good for food; the tree of life also in the midst of the garden, and the tree of knowledge of good and evil.", "David", "synagogue"};
		
		for(String verse: verses) {
			System.out.println(bibleVerse.search(verse));
		}
		
		
		String[] locations = {"Genesis 3:4", "Joshua 24:22","Judges 1:11", "Judges 1:12", "Judges 1:13", "Judges 1:36","Judges 8:31", "Revelation 7:7"};
		
		int i, size = locations.length;
		
		for(i = 0; i < size; i++ ) {
//			System.out.println("searchByLocation->" + locations[i] + " " + bibleVerse.searchByLocation(locations[i], bibleVerse));
			System.out.println("getVerseByLocation->" + locations[i] + " " + bibleVerse.getVerseByLocation(locations[i], bibleVerse));
		}
		
//		System.out.println(testAllVerseLocation(bibleVerse));
		
		System.out.println(testSearchVerse(bibleVerse));
	}
	
	public TriesDictionary.Node parseBibleWord() {

		TriesDictionary.Node bible = new Node();
		
		ReadFile readFile = new ReadFile();
		HashMap<String, String> map = readFile.readWordFromFile();
		
		System.out.println(map.size());
		
		TriesDictionary tries = new TriesDictionary();
		for(String key : map.keySet()) {
			bible.insertWord(key);
		}

		TriesDictionary.Node result = tries.getDictionary();
		return result;
	}
	
	
	public TriesSentence.Node parseBibleVerse() {

		TriesSentence.Node bible = new TriesSentence.Node();
		
		ReadFile readFile = new ReadFile();
		HashMap<String, String> map = readFile.readVerseFromFile();
		
		System.out.println(map.size());
		
		TriesSentence tries = new TriesSentence();
		for(String key : map.keySet()) {
			bible.insertVerse(key, map.get(key));
		}

		TriesSentence.Node result = tries.getDictionary();
		return result;
	}	
	
	
	public boolean testAllVerseLocation(TriesSentence.Node bibleVerse) {
		
		ReadFile readFile = new ReadFile();
		HashMap<String, String> map = readFile.readVerseLocationFromFile();
		
		System.out.println("Location Size ---------->" + map.size());
		
		TriesSentence.Node locatedNode = null;
		int foundCount = 0;
		
		for(String key : map.keySet()) {
			locatedNode = bibleVerse.searchByLocation(key, bibleVerse);
			if(locatedNode != null) {
				foundCount++;
			}
			if(foundCount % 100 == 0) {
				System.out.println("current foundCount=" + foundCount);
			}
		}
		System.out.println("foundCount ---------->" + foundCount);

		return foundCount == map.size();
	}	

	public boolean testSearchVerse(TriesSentence.Node bibleVerse) {
		
		ReadFile readFile = new ReadFile();
		HashMap<String, String> map = readFile.readSearchVersenFromFile();
		
		System.out.println("Location Size ---------->" + map.size());
		
		TriesSentence.Node locatedNode = null;
		int foundCount = 0;
		boolean found = false;
		
		for(String key : map.keySet()) {
			found = bibleVerse.search(key);
			if(found) {
				foundCount++;
			}
			if(foundCount % 100 == 0) {
				System.out.println("current foundCount=" + foundCount);
			}
		}
		System.out.println("foundCount ---------->" + foundCount);

		return foundCount == map.size();
	}		
	
	
}
